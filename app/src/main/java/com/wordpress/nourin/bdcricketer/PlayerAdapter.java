package com.wordpress.nourin.bdcricketer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class PlayerAdapter extends ArrayAdapter {

    List<Player> playerList = new ArrayList<Player>();


    public PlayerAdapter(@NonNull Context context, List<Player> playerList) {
        super(context, R.layout.item,playerList);
        this.playerList = playerList;

    }




    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item,parent,false);

        TextView tvName = view.findViewById(R.id.tv_item_name);
        ImageView imgProfile = view.findViewById(R.id.iv_item_image);
        TextView tvType = view.findViewById(R.id.tv_item_type);


        tvName.setText(playerList.get(position).getmName());
        tvType.setText(playerList.get(position).getmType());
        imgProfile.setImageResource(playerList.get(position).getmImage());

        return view;
    }


}
