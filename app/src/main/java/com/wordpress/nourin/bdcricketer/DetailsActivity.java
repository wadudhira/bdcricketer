package com.wordpress.nourin.bdcricketer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import static com.wordpress.nourin.bdcricketer.MainActivity.playerList;

public class DetailsActivity extends AppCompatActivity {
    ImageView ivProfilepic;
    TextView tvName, tvType,tvData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        int position = getIntent().getExtras().getInt("position_id");
        ivProfilepic = (ImageView) findViewById(R.id.iv_activity_details_image);
        tvName = (TextView)findViewById(R.id.tv_activity_details_name);
        tvType = (TextView)findViewById(R.id.tv_activity_details_type);
        tvData = (TextView)findViewById(R.id.tv_activity_details_data);

        ivProfilepic.setImageResource(playerList.get(position).getmImage());
        tvName.setText(playerList.get(position).getmName());
        tvType.setText(playerList.get(position).getmType());
        tvData.setText(playerList.get(position).getmData());

    }
}
