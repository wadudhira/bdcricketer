package com.wordpress.nourin.bdcricketer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


        ListView lvPlayerListview;
        PlayerAdapter playerAdapter;
      public static List<Player> playerList = new ArrayList<Player>();

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            lvPlayerListview = (ListView)findViewById(R.id.lv_activity_main_playerlist);

            playerList.add(new Player(R.drawable.masrafi,"MASRAFI","Bowler","Popularly known as the 'Narail Express', Mortaza's career has been beset by injuries. The first real quick bowler to come out of Bangladesh, he was earmarked for great things since the time he made his test debut against Zimbabwe in 2001-02. Stockily built, he could generate uncomfortable bounce with his strong action. \n" +
                    "\n" +
                    "He impressed Andy Roberts who was then coaching Bangladesh and was drafted into the Test side - in what was his debut first-class game as well - he was the 31st person to have achieved this, and the third since 1899. After his Test debut, injuries started wreaking havoc and he found himself making one comeback after the other. Injuries to his back and knee forced him to miss most of the action, but came back and broke Aftab Ahmed's national record, en-route to taking 6/26 against Kenya in an ODI. \n" +
                    "\n" +
                    "In the calendar year of 2006, Mortaza was the world's highest wicket taker in ODIs, collecting 49 wickets. The haul was the most ever in a calendar year by a Bangladeshi, and 17th most for anyone. He was the Man of the Match in his side's shock victory over India in the 2007 World Cup taking 4/38. Unsurprisingly he played in all of Bangladesh's matches in the World Cup as they reached the second round. \n" +
                    "\n" +
                    "Mortaza's strengths are hitting the deck and getting appreciable movement off the seam. When he is at his peak, he was capable of surprising the batsmen with his sheer pace, but constant injuries have meant that he has remodeled his action and relies mostly on bowling a restrictive line with an emphasis on eking out an error from the batsmen. \n" +
                    "\n" +
                    "A capable lower order batsman, who can use the long handle to good effect, Mortaza was picked up by the KKR side $600,000 in February 2009. He had a shock debut with them when he conceded 26 runs in the last over to Rohit Sharma when 21 was required who snatched an almost impossible win. That was the only game he played in the season as the franchisee consigned him to the benches. \n" +
                    "\n" +
                    "Mortaza was appointed captain after the T20 World Cup in the Caribbean. He was to debut as captain when Bangladesh toured West Indies and Zimbabwe, but an ankle injury forced him to hand over the captaincy duties to Shakib. He suffered a tear in his knee ligament which forced him to miss the 2011 World Cup. He hoped to recover in time, and when it was announced that Mortaza had not been included in the World Cup squad there was rioting in Bangladesh and in one place a half-day strike. \n" +
                    "\n" +
                    "The Bangladesh Cricket Board started the Bangladesh Premier League in 2012, a Twenty20 tournament to be held in February 2012. In the player auction Mortaza was snapped up by the Dhaka Gladiators for 45,000 USD. On his return to competitive cricket, Mortaza was made the captain of Dhaka Gladiators. Before the tournament had started, he reported to have been approached with a request to participate in spot-fixing. Mortaza led his team Dhaka Gladiators to the title in the inaugural edition of BPL, where he took ten wickets from eleven matches. \n" +
                    "\n" +
                    "Shortly after BPL concluded, Bangladesh hosted the 2012 Asia Cup. After losing to Pakistan the previous December, five new players were called into Bangladesh's 15-man squad, including Mortaza who was making his comeback to the national side after an injury. When the BCB announced its central contracts for 2012 in March, Mortaza retained his top level contract. Bangladesh exceeded all expectations and progressed to the final against Pakistan, it was just the second time that Bangladesh had reached the final of a tournament. Though he had played in the BPL, bowling ten overs in the ODIs proved to be a challenge for Mortaza. He struggled to bowl his quota of 10 overs, but finished as Bangladesh's leading seamer with six wickets from four matches. Even after being in and out of the squad Mortaza was able to retain his top level central contract with BCB in March 2012. "));
            playerList.add(new Player(R.drawable.musfiq,"MUSFIQUR","Batsman",""));
            playerList.add(new Player(R.drawable.sakib,"SAKIB","Batsman",""));
            playerList.add(new Player(R.drawable.sabbir,"SABBIR","Batsmant",""));
            playerList.add(new Player(R.drawable.tamim,"TAMIM","Batsman",""));


            playerAdapter = new PlayerAdapter(getApplicationContext(),playerList);
            lvPlayerListview.setAdapter(playerAdapter);
           lvPlayerListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
               @Override
               public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                   Intent intent = new Intent(getApplicationContext(),DetailsActivity.class);
                   intent.putExtra("position_id",position);
                   startActivity(intent);
               }
           });
        }
    }
