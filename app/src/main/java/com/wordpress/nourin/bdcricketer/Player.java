package com.wordpress.nourin.bdcricketer;

public class Player {
    int mImage;
    String mName,mType,mData;

    public Player() {
    }

    public Player(int mImage, String mName, String mType,String mData) {
        this.mImage = mImage;
        this.mName = mName;
        this.mType = mType;
        this.mData=mData;
    }

    public int getmImage() {
        return mImage;
    }

    public void setmImage(int mImage) {
        this.mImage = mImage;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }
    public String getmData() {
        return mData;
    }

    public void setmData(String mData) {
        this.mData = mData;
    }

}
